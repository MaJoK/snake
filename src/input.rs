use termion::event::Key;
use termion::input::Keys;

pub struct Input<'a, T> {
    input_time: u16,
    pre_input_delay: u16,
    key_source: &'a mut Keys<T>,
    queued_key: Option<Key>, // Key from previous run
}

impl<'a, T: Sized + std::io::Read> Input<'a, T> {
    pub fn new(keys: &'a mut Keys<T>, input_time: u16) -> Self {
        Self {
            input_time,
            pre_input_delay: 0,
            key_source: keys,
            queued_key: None,
        }
    }

    pub fn next_input(&mut self, quick_return: impl Fn(Key) -> bool) -> Option<Key> {
        self.wait_pre_input_delay();
        let mut pressed_key = self.drain_keys();

        let mut total_slept: u16 = 0;
        while total_slept < self.input_time {
            if let Some(event) = self.key_source.next() {
                pressed_key = Some(event.expect("Error reading from the tty"));
            }
            if let Some(new_key) = pressed_key {
                match self.queued_key {
                    Some(key) => {
                        // If there is already a key "queued" we queue this one and process the queued one first
                        let key_to_use = key;
                        self.queued_key = Some(new_key);
                        if !quick_return(new_key) {
                            // Before the next input some time should be waited
                            self.pre_input_delay = self.input_time - total_slept;
                        }
                        return Some(key_to_use);
                    }
                    // Otherwise queue this one
                    None => {
                        if quick_return(new_key) {
                            return Some(new_key);
                        } else {
                            self.queued_key = Some(new_key)
                        }
                    }
                }
                pressed_key = None;
            }
            let sleep_time = 1;
            sleep(sleep_time);
            total_slept += sleep_time;
        }
        // If no other input was given we use the key that was entered but wasn't used yet
        return self.queued_key.take();
    }

    fn wait_pre_input_delay(&mut self) {
        if self.pre_input_delay != 0 {
            sleep(self.pre_input_delay);
            self.pre_input_delay = 0;
        }
    }

    fn drain_keys(&mut self) -> Option<Key> {
        let mut last_key = None;
        while let Some(event) = self.key_source.next() {
            last_key = Some(event.expect("Error reading from the tty"));
        }
        last_key
    }
}

fn sleep(ms: u16) {
    std::thread::sleep(std::time::Duration::from_millis(ms.into()));
}
