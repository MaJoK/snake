use std::io;
use std::io::Write;

use termion::color::Color as Colour;
use termion::color::{Bg, Fg, Reset};
use termion::cursor::HideCursor;
use termion::event::Key;
use termion::input::{Keys, TermRead};
use termion::raw::{IntoRawMode, RawTerminal};

use clap::{crate_version, App, Arg, ArgMatches, SubCommand};

use std::thread::sleep;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

use game::{Direction, EdgeMode, GameState};

fn main() {
    let matches = build_app().get_matches();

    let license = matches.is_present("license");

    if license {
        print_license();
        return;
    }

    if let ("completions", Some(sub_matches)) = matches.subcommand() {
        print_shell_completion(&sub_matches);
        return;
    }

    let speed = matches.value_of("speed").unwrap().parse::<u16>().unwrap();
    let width = matches
        .value_of("width")
        .map(|string| string.parse::<u16>().unwrap());
    let height = matches
        .value_of("height")
        .map(|string| string.parse::<u16>().unwrap());
    let apples = matches.value_of("apples").unwrap().parse::<u16>().unwrap();

    let walls_enabled = matches.is_present("walls");
    let borders_disabled = matches.is_present("noborders");

    let draw_config = DrawConfiguration::initiate();

    let drawing_size = get_available_game_size(width, height);

    let mut game = game::initialize_game(
        &drawing_size,
        apples,
        determine_edge_mode(walls_enabled, borders_disabled),
    );

    let mut input = termion::async_stdin().keys();
    run_game(&mut game, speed, &mut input);

    draw_config.restore();

    print_score(&game);
}

mod game;
mod input;

fn print_score(game: &game::Game) {
    reset_colours();
    println!("\n\nScore: {}", game.score());
}

fn number_between(min: i16, max: i16) -> impl Fn(String) -> Result<(), String> + 'static {
    move |string| {
        let num = string.parse::<i16>();
        if let Ok(num) = num {
            if num < min || num > max {
                Err(format!("needs to be between {} and {}", min, max))
            } else {
                Ok(())
            }
        } else {
            Err("whole number expected".to_string())
        }
    }
}

fn to_direction(key: Key) -> Option<Direction> {
    match key {
        Key::Left | Key::Char('h') | Key::Char('a') => Some(Direction::West),
        Key::Up | Key::Char('k') | Key::Char('w') => Some(Direction::North),
        Key::Right | Key::Char('l') | Key::Char('d') => Some(Direction::East),
        Key::Down | Key::Char('j') | Key::Char('s') => Some(Direction::South),
        _ => None,
    }
}

fn run_game<T: Sized + io::Read>(game: &mut game::Game, speed: u16, input: &mut Keys<T>) {
    let mut input = input::Input::new(input, 1000 / speed);
    'main_loop: loop {
        let mut new_direction = None;
        if let Some(key) = input.next_input(|key| {
            if let Some(direction) = to_direction(key) {
                // When moving in the same direction again
                direction == game.current_direction()
            } else {
                false
            }
        }) {
            if let Some(direction) = to_direction(key) {
                new_direction = Some(direction);
            } else {
                match key {
                    Key::Ctrl('c') | Key::Char('q') => break 'main_loop,
                    Key::Char('p') => match pause_game(game, &mut input) {
                        PauseResult::Continue => (),
                        PauseResult::Quit => break 'main_loop,
                    },
                    _ => (),
                }
            }
        }
        match game.next_state(new_direction) {
            GameState::Continue => (),
            GameState::GameOver => break 'main_loop,
        }
    }
}

enum PauseResult {
    Continue,
    Quit,
}

fn pause_game<T: Sized + io::Read>(
    game: &mut game::Game,
    input: &mut input::Input<T>,
) -> PauseResult {
    game.pause_with_message("Game paused. Press p to resume");
    loop {
        while let Some(key) = input.next_input(|_key| false) {
            match key {
                Key::Char('p') => {
                    game.unpause();
                    return PauseResult::Continue;
                }
                Key::Ctrl('c') | Key::Char('q') => {
                    return PauseResult::Quit;
                }
                _ => (),
            }
        }
        sleep(Duration::from_millis(1));
    }
}
pub struct DrawingSize {
    width: u16,
    height: u16,
}

fn get_available_game_size(arg_width: Option<u16>, arg_height: Option<u16>) -> DrawingSize {
    let width_to_use: u16;
    let height_to_use: u16;

    if arg_width.is_none() || arg_height.is_none() {
        let (width, height) = termion::terminal_size().expect("Couldn't get terminal size");
        width_to_use = arg_width.unwrap_or(width) / 2;
        height_to_use = arg_height.unwrap_or(height);
    } else {
        width_to_use = arg_width.unwrap();
        height_to_use = arg_height.unwrap();
    }

    DrawingSize {
        width: width_to_use,
        height: height_to_use,
    }
}

fn reset_colours() {
    print!("{}{}", Fg(Reset), Bg(Reset));
}

fn clear_screen() {
    reset_colours();
    print!("{}", termion::clear::All);
}

fn flush_out() {
    std::io::stdout().flush().unwrap();
}

fn set_bg_colour<T: Colour>(colour: Bg<T>) {
    print!("{}", colour);
}

fn stroke_position(x: u16, y: u16) {
    print!("{}  ", termion::cursor::Goto((x * 2) + 1, y + 1)); //tty starts at coordinates 1,1
}

fn print_at_position(x: u16, y: u16, left_char: char, right_char: char) {
    print!(
        "{}{}{}",
        termion::cursor::Goto((x * 2) + 1, y + 1),
        left_char,
        right_char
    );
}

fn horizontal_line(x: u16, y: u16, len: u16, left_char: char, right_char: char) {
    let mut chars = String::with_capacity(2);
    chars.push(left_char);
    chars.push(right_char);
    print!(
        "{}{}",
        termion::cursor::Goto((x * 2) + 1, y + 1),
        chars.repeat(len as usize),
    );
}

/// Prints the given string centered at the given coordinates
fn print_centered_at(x: u16, y: u16, string: &str) {
    let x = (x * 2) - string.len() as u16 / 2;
    print!("{}{}", termion::cursor::Goto(x + 1, y + 1), string);
}

struct DrawConfiguration {
    #[allow(dead_code)]
    raw_mode: RawTerminal<io::Stdout>,
    #[allow(dead_code)]
    cursor_hidden: HideCursor<io::Stdout>,
}

impl DrawConfiguration {
    fn initiate() -> Self {
        Self {
            raw_mode: io::stdout().into_raw_mode().unwrap(),
            cursor_hidden: HideCursor::from(io::stdout()),
        }
    }

    fn restore(self) {
        // Dropping this restores the previous config
        std::mem::drop(self);
    }
}

fn time_since_epoch() -> u64 {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    since_the_epoch.as_secs()
}

fn determine_edge_mode(walls: bool, no_borders: bool) -> EdgeMode {
    if walls {
        EdgeMode::Walled
    } else if no_borders {
        EdgeMode::Free
    } else {
        EdgeMode::Bordered
    }
}

fn build_app() -> App<'static, 'static> {
    App::new("snake")
        .author("Mark Joachim Krallmann joachim.krallmann@protonmail.com")
        .about("A snake game")
        .version(crate_version!())
        .arg(Arg::with_name("speed")
                .short("s")
                .long("speed")
                .help("sets the game speed, valid: 1-1000")
                .default_value("10")
                .validator(number_between(1, 1000))
                .takes_value(true))
        .arg(Arg::with_name("height")
                .short("h")
                .long("height")
                .help("Sets the height the game will use, defaults to the full height of the terminal")
                .validator(number_between(10, 1000))
                .takes_value(true))
        .arg(Arg::with_name("width")
                .short("w")
                .long("width")
                .help("Sets the width the game will use, defaults to the full width of the terminal")
                .validator(number_between(10, 1000))
                .takes_value(true))
        .arg(Arg::with_name("apples")
                .short("a")
                .long("apples")
                .help("The amount of apples present at all times")
                .default_value("2")
                .validator(number_between(0, 1000))
                .takes_value(true))
        .arg(Arg::with_name("walls")
                .long("walls")
                .help("When passed runs the game with impassable edges enabled. Overrides --noborders"))
        .arg(Arg::with_name("noborders")
                .long("noborders")
                .help("When passed runs the game without borders"))
        .arg(Arg::with_name("license")
                .short("l")
                .long("license")
                .help("Prints license information and quits"))
        .subcommand(SubCommand::with_name("completions")
                        .about("Generate shell completion script and print it to stdout")
                        .arg(Arg::with_name("shell")
                                .required(true)
                                .possible_values(&["bash", "fish", "zsh"])
                                .help("The shell to generate the script for")
                        )
        )
        .after_help("Use the wasd keys, hjkl keys or arrow keys to control the snake")
}
fn print_license() {
    println!(
        "
    snake
    Copyright (C) 2019  Mark Joachim Krallmann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."
    )
}

fn print_shell_completion<'a>(sub_matches: &ArgMatches<'a>) {
    let shell = sub_matches.value_of("shell").unwrap();
    build_app().gen_completions_to("snake", shell.parse().unwrap(), &mut io::stdout());
}
