use super::super::{
    horizontal_line, print_at_position, set_bg_colour, stroke_position, DrawingSize,
};
use super::snake::{self, Snake, WrapMode};
use super::{Apple, Direction, Position};
use std::collections::HashMap;

use termion::color::{Bg, Green, LightBlack};

pub struct Garden {
    snake: Snake,
    apples: HashMap<Position, Apple>,
    width: u16,
    height: u16,
    mode: GardenMode,
}

pub enum MoveResult {
    Okay { ate_apple: bool },
    HeadInWall,
    SnakeAteItself,
}

#[derive(Clone, Copy)]
pub enum GardenMode {
    Walled,
    Bordered,
    Free,
}

impl Garden {
    pub fn new(available_size: &DrawingSize, mode: GardenMode) -> Self {
        let wall_factor = if let GardenMode::Free = mode { 0 } else { 2 };

        let height = available_size.height - WALL_THICKNESS_TOP_BOTTOM * wall_factor;
        let width = available_size.width - WALL_THICKNESS_LEFT_RIGHT * wall_factor;

        let snake = Snake::new(
            3,
            Position { x: 3, y: 1 },
            match mode {
                GardenMode::Walled => WrapMode::NoWrap,
                GardenMode::Bordered | GardenMode::Free => WrapMode::Wrap { height, width },
            },
        );

        Garden {
            snake,
            apples: HashMap::new(),
            width,
            height,
            mode,
        }
    }

    pub fn move_snake(&mut self, direction: Direction) -> MoveResult {
        let move_result = self.snake.move_in_direction(direction);
        if let snake::MoveResult::AteSelf = move_result {
            return MoveResult::SnakeAteItself;
        }

        let snake_head = self.snake.get_head_position();

        if self.is_wall(snake_head) {
            return MoveResult::HeadInWall;
        }

        if self.has_apple_on_position(snake_head) {
            self.apples.remove(&snake_head);
            self.snake.you_ate_apple();
            MoveResult::Okay { ate_apple: true }
        } else {
            MoveResult::Okay { ate_apple: false }
        }
    }

    fn is_wall(&self, position: Position) -> bool {
        if let GardenMode::Bordered = self.mode {
            return false;
        };

        if position.x < 0 || position.y < 0 {
            true
        } else {
            self.width as i32 <= position.x || self.height as i32 <= position.y
        }
    }

    fn has_apple_on_position(&self, position: Position) -> bool {
        self.apples.contains_key(&position)
    }

    pub fn new_random_apple(&mut self) {
        let mut position = Position::random_within(self.width as u32, self.height as u32);
        let mut tries = 1;
        while self.has_apple_on_position(position) || self.snake.get_head_position() == position {
            position = Position::random_within(self.width as u32, self.height as u32);
            tries += 1;
            if tries > 100 {
                panic!("Tried placing an apple at least 100 times without success");
            }
        }

        self.add_apple(position, Apple);
    }

    fn add_apple(&mut self, position: Position, apple: Apple) {
        self.apples.insert(position, apple);
    }
}

const WALL_BACKGROUND_COLOUR: Bg<LightBlack> = Bg(LightBlack);
pub const WALL_THICKNESS_LEFT_RIGHT: u16 = 1;
pub const WALL_THICKNESS_TOP_BOTTOM: u16 = 1;
impl Garden {
    pub fn print_yourself(&self) {
        if let GardenMode::Free = self.mode {
            self.print_apples(0, 0);
            self.snake.print_yourself(0, 0);
        } else {
            self.print_walls();
            // Print with offsets
            self.print_apples(WALL_THICKNESS_LEFT_RIGHT, WALL_THICKNESS_TOP_BOTTOM);
            self.snake
                .print_yourself(WALL_THICKNESS_LEFT_RIGHT, WALL_THICKNESS_TOP_BOTTOM);
        }
    }

    fn print_walls(&self) {
        set_bg_colour(WALL_BACKGROUND_COLOUR);
        // Upper line
        let last_column = self.width + (WALL_THICKNESS_LEFT_RIGHT * 2) - 1;
        let top_bottom_length = self.width + (WALL_THICKNESS_LEFT_RIGHT * 2);

        let horizontal_inner = match self.mode {
            GardenMode::Walled => ('━', '━'),
            _ => (' ', ' '),
        };

        let bottom_border_line = self.height + 1;
        // Leave left and right columns free
        horizontal_line(
            1,
            0,
            top_bottom_length - 1,
            horizontal_inner.0,
            horizontal_inner.1,
        );
        // Leave left and right columns free
        horizontal_line(
            1,
            bottom_border_line,
            top_bottom_length - 1,
            horizontal_inner.0,
            horizontal_inner.1,
        );

        let vertical_inner = match self.mode {
            GardenMode::Walled => ('┃', ' '),
            _ => (' ', ' '),
        };

        // Borders left and right
        for line in 1..=self.height {
            print_at_position(0, line, vertical_inner.0, vertical_inner.1);
            // Print reversed on the other side
            print_at_position(last_column, line, vertical_inner.1, vertical_inner.0);
        }

        match self.mode {
            GardenMode::Walled => {
                print_at_position(0, 0, '┏', '━'); // Top left
                print_at_position(last_column, 0, '━', '┓'); // Top right
                print_at_position(0, bottom_border_line, '┗', '━'); // Bottom left
                print_at_position(last_column, bottom_border_line, '━', '┛'); // Bottom right
            }
            _ => {
                print_at_position(0, 0, ' ', ' '); // Top left
                print_at_position(0, last_column, ' ', ' '); // Top right
                print_at_position(0, bottom_border_line, ' ', ' '); // Bottem left
                print_at_position(last_column, bottom_border_line, ' ', ' '); // Bottem right
            }
        }
    }

    fn print_apples(&self, x_offset: u16, y_offset: u16) {
        set_bg_colour(Bg(Green));
        for position in self.apples.keys() {
            stroke_position(position.x as u16 + x_offset, position.y as u16 + y_offset);
        }
    }
}
