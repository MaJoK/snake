mod garden;
mod snake;

use garden::{Garden, GardenMode, MoveResult};

use super::{
    clear_screen, flush_out, print_centered_at, reset_colours, time_since_epoch, DrawingSize,
};

use oorandom::Rand32;

use std::cell::RefCell;

pub fn initialize_game(drawing_size: &DrawingSize, apples: u16, egdges: EdgeMode) -> Game {
    let garden = Garden::new(drawing_size, egdges.into());

    Game::new(garden, apples, drawing_size)
}

pub enum GameState {
    Continue,
    GameOver,
}

pub enum EdgeMode {
    Bordered,
    Walled,
    Free,
}

impl Into<GardenMode> for EdgeMode {
    fn into(self) -> GardenMode {
        match self {
            EdgeMode::Bordered => GardenMode::Bordered,
            EdgeMode::Walled => GardenMode::Walled,
            EdgeMode::Free => GardenMode::Free,
        }
    }
}

struct DrawCenter {
    x: u16,
    y: u16,
}

impl DrawCenter {
    fn determine(drawing_size: &DrawingSize) -> Self {
        Self {
            x: drawing_size.width / 2,
            y: drawing_size.height / 2,
        }
    }
}

pub struct Game {
    garden: Garden,
    move_direction: Direction,
    score: u16,
    draw_center: DrawCenter,
}

impl Game {
    fn new(mut garden: Garden, apples: u16, drawing_size: &DrawingSize) -> Self {
        for _ in 0..apples {
            garden.new_random_apple();
        }
        Self {
            garden,
            move_direction: Direction::East,
            score: 0,
            draw_center: DrawCenter::determine(drawing_size),
        }
    }

    pub fn score(&self) -> u16 {
        self.score
    }

    pub fn next_state(&mut self, move_direction: Option<Direction>) -> GameState {
        if let Some(direction) = move_direction {
            if direction != self.move_direction.opposite() {
                self.move_direction = direction;
            }
        }
        let result = self.garden.move_snake(self.move_direction);
        clear_screen();
        self.print_score();
        self.garden.print_yourself();
        flush_out();
        match result {
            MoveResult::Okay { ate_apple } => {
                if ate_apple {
                    self.garden.new_random_apple();
                    self.score += 1;
                }
                GameState::Continue
            }
            MoveResult::HeadInWall | MoveResult::SnakeAteItself => GameState::GameOver,
        }
    }

    pub fn pause_with_message(&self, message: &str) {
        clear_screen();
        self.garden.print_yourself();
        reset_colours();
        print_centered_at(self.draw_center.x, self.draw_center.y, message);
        flush_out();
    }

    pub fn unpause(&self) {}

    fn print_score(&self) {
        reset_colours();
        print_centered_at(
            self.draw_center.x,
            self.draw_center.y,
            &format!("Score: {}", self.score),
        );
    }

    pub fn current_direction(&self) -> Direction {
        self.move_direction
    }
}

#[derive(Hash, PartialEq, Eq, Copy, Clone, Debug)]
pub struct Position {
    x: i32,
    y: i32,
}

impl Position {
    fn offset_by(self, x_offset: i32, y_offset: i32) -> Self {
        Self {
            x: self.x + x_offset,
            y: self.y + y_offset,
        }
    }

    fn random_within(max_x: u32, max_y: u32) -> Self {
        Self {
            x: (random_num() % max_x) as i32,
            y: (random_num() % max_y) as i32,
        }
    }

    fn offset_by_with_wrap(self, x_offset: i32, y_offset: i32, max_x: u32, max_y: u32) -> Self {
        Self {
            x: Position::keep_between(self.x, x_offset, max_x),
            y: Position::keep_between(self.y, y_offset, max_y),
        }
    }

    fn keep_between(value: i32, to_add: i32, wrap_around_to: u32) -> i32 {
        let wrap_around_to = wrap_around_to as i32;
        let value = value + to_add;
        (value % wrap_around_to + wrap_around_to) % wrap_around_to
    }
}

thread_local! {
    static RNG: RefCell<Rand32> = RefCell::new(Rand32::new(time_since_epoch()));
}

fn random_num() -> u32 {
    RNG.with(|rng| rng.borrow_mut().rand_u32())
}

#[derive(PartialEq, Debug, Copy, Clone)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn offset(self, steps: i32, position: Position) -> Position {
        match self {
            Direction::North => position.offset_by(0, -steps),
            Direction::East => position.offset_by(steps, 0),
            Direction::South => position.offset_by(0, steps),
            Direction::West => position.offset_by(-steps, 0),
        }
    }

    fn offset_with_wrap(
        self,
        steps: i32,
        position: Position,
        x_wrap: u32,
        y_wrap: u32,
    ) -> Position {
        match self {
            Direction::North => position.offset_by_with_wrap(0, -steps, x_wrap, y_wrap),
            Direction::East => position.offset_by_with_wrap(steps, 0, x_wrap, y_wrap),
            Direction::South => position.offset_by_with_wrap(0, steps, x_wrap, y_wrap),
            Direction::West => position.offset_by_with_wrap(-steps, 0, x_wrap, y_wrap),
        }
    }

    fn opposite(self) -> Direction {
        match self {
            Direction::North => Direction::South,
            Direction::East => Direction::West,
            Direction::South => Direction::North,
            Direction::West => Direction::East,
        }
    }
}

struct Apple;

#[cfg(test)]
mod test {
    use super::Position;

    #[test]
    fn test_direction_offset() {
        assert_eq!(
            Position { x: 5, y: 4 },
            super::Direction::North.offset(1, Position { x: 5, y: 5 })
        );
        assert_eq!(
            Position { x: 6, y: 5 },
            super::Direction::East.offset(1, Position { x: 5, y: 5 })
        );
        assert_eq!(
            Position { x: 5, y: 6 },
            super::Direction::South.offset(1, Position { x: 5, y: 5 })
        );
        assert_eq!(
            Position { x: 4, y: 5 },
            super::Direction::West.offset(1, Position { x: 5, y: 5 })
        );
    }

    #[test]
    fn test_direction_offset_with_wrap() {
        assert_eq!(
            Position { x: 5, y: 9 },
            super::Direction::North.offset_with_wrap(6, Position { x: 5, y: 5 }, 10, 10)
        );
        assert_eq!(
            Position { x: 1, y: 5 },
            super::Direction::East.offset_with_wrap(6, Position { x: 5, y: 5 }, 10, 10)
        );
        assert_eq!(
            Position { x: 5, y: 1 },
            super::Direction::South.offset_with_wrap(6, Position { x: 5, y: 5 }, 10, 10)
        );
        assert_eq!(
            Position { x: 9, y: 5 },
            super::Direction::West.offset_with_wrap(6, Position { x: 5, y: 5 }, 10, 10)
        );
    }
}
