use std::collections::VecDeque;

use super::super::{print_at_position, set_bg_colour, stroke_position};
use super::{Direction, Position};

pub struct Snake {
    positions: VecDeque<Position>,
    ate_apple: bool,
    facing: Direction,
    wrap_mode: WrapMode,
}

pub enum MoveResult {
    Ok,
    AteSelf,
}

pub enum WrapMode {
    NoWrap,
    Wrap { width: u16, height: u16 },
}

impl Snake {
    pub fn new(start_size: u32, start_head_position: Position, wrap_mode: WrapMode) -> Self {
        let mut positions = VecDeque::new();

        let tail_direction = Direction::West;
        let mut last_position = start_head_position;
        positions.push_back(start_head_position);

        for _ in 1..=start_size {
            last_position = tail_direction.offset(1, last_position);
            positions.push_front(last_position);
        }

        Self {
            positions,
            ate_apple: false,
            facing: tail_direction.opposite(),
            wrap_mode,
        }
    }

    pub fn you_ate_apple(&mut self) {
        self.ate_apple = true;
    }

    pub fn get_head_position(&self) -> Position {
        *self.positions.back().unwrap()
    }

    #[must_use]
    pub fn move_in_direction(&mut self, direction: Direction) -> MoveResult {
        let head = self.get_head_position();

        if !self.ate_apple {
            self.positions.pop_front();
        } else {
            self.ate_apple = false;
        }

        let new_head = match self.wrap_mode {
            WrapMode::Wrap { height, width } => {
                direction.offset_with_wrap(1, head, width as u32, height as u32)
            }
            WrapMode::NoWrap => direction.offset(1, head),
        };

        let ate_self = self.positions.contains(&new_head);

        self.positions.push_back(new_head);
        self.facing = direction;

        if ate_self {
            MoveResult::AteSelf
        } else {
            MoveResult::Ok
        }
    }
}

const SNAKE_BACKGROUND_COLOUR: termion::color::Bg<termion::color::Red> =
    termion::color::Bg(termion::color::Red);

impl Snake {
    pub fn print_yourself(&self, x_offset: u16, y_offset: u16) {
        set_bg_colour(SNAKE_BACKGROUND_COLOUR);
        let to_x = |pos_x: i32| (x_offset as i32 + pos_x) as u16;
        let to_y = |pos_y: i32| (y_offset as i32 + pos_y) as u16;

        for pos in &self.positions {
            stroke_position(to_x(pos.x), to_y(pos.y));
        }

        let head = self.get_head_position();
        let head_chars = chars_for_face(self.facing);
        print_at_position(to_x(head.x), to_y(head.y), head_chars.0, head_chars.1);
    }
}

fn chars_for_face(direction: Direction) -> (char, char) {
    match direction {
        Direction::North | Direction::South => ('⋅', '⋅'),
        Direction::West => (':', ' '),
        Direction::East => (' ', ':'),
    }
}
