## About

My implementation of snake in Rust.


The game running inside tilix:  
![](snake.jpg)


## Building

0. Install cargo
1. `cargo install --git https://gitlab.com/MaJoK/snake.git`

The snake binary should be in your path now

## Executing  

Run the snake binary inside a tty. Not sure whether this works on Windows. It does work on Linux and should also on BSD and OSX and Redox (WSL probably too)
There are some options. Pass the --help parameter to see them